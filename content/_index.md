# Hello from the front page!

This is a markdown file that become HTML because hugo is awesome like that.

I know this doesn't look like much (yet, we can code jam some ideas)  
but a "blank slate" website is a really powerfull tool.

## Ideas!
Before we get started looking through the site files and learning how to
make modifications to it, lets list off some ways you might want to use
this site:  

• Online gallery (or portfolio)  
• Social networking  
• Start and manage a school club  
• Create your first resume! (Websites can be downloaded as PDFs)  
• Create digital products (and sell them!)  
• Write a blog (and monotize it!)  

There are many possibilities.  

In theory, we could do more than one of those things (or all of them) with this one site!  

Having a goal in mind will help you decide on how you'd like to design the site. There are
several other tools that will help with this as well, like [Figma.](https://www.figma.com)

## And Now! Documentation:
First things first, read the damn README.  
**Pro tip:** One can save themselvs several minutes of reading the README with several hours of trial and error.  
There's no way around it, we gonna' be rrrreeeeeeeeaading ... a lot.

[README](https://gitlab.com/dsbarnes/ayannah/-/blob/main/README.md)
