---
title: {{ replace .File.TranslationBaseName "-" " " | title }}
subtitle:
date: {{ .Date }}
description: Twitter Card and OG Text
draft: true

categories: []
tags: []
series:  []

videos: []
audio: []
images: []
---
