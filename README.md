# Welcome To Your Website!
This started out as a far less complicated project than it is now.
However, let me assure you that everything you covered in your CS class
was far more than enough to understand how this site functions.  


# Who the hell is Hugo?
[Hugo](https://gohugo.io/) is a framework for building websites.  

 Hugo is built with a programming language called [Go](https://go.dev/).  
This is important because Hugo uses Go's [templating language](https://docs.gomplate.ca/syntax/),
which helps us reuse HTML.  

(More specific docs [here](https://gohugo.io/templates/introduction/))  

This of course begs the question, why would anyone care about
reusable HTML?  

The most obvious reason is that it's extremely repetitive and time consuming
to rewrite section of a website that appear on every page, for example
a navigation menu.  

There are other more advanced reason, like social sharing descriptions,
title tags, and lots of SEO related concepts.  


# Ok, so what do?
Unfortunately the first couple steps are going to be somewhat technical and
just painful enough to cause some frustration, so prepare for that.  
Even more unfortunate, the steps will be slightly different if you use a 
windows, mac, or linux OS.  

Slow is smooth, smooth is fast.  


## VS Code
There are two programs. Microsoct Visual Studio (Not what we want), and 
Microsoft Visual Studio Code (we want that).  
Notice one has the word 'code' and the other doesn't.  
Microsoft is really, and I mean REALLY shit at naming things.  
There is, for example, a 'teams' tab in their 'teams' app.  

Don't worry right now about any plugins or whatever, they will be suggested
as you open files in the VS Code application.

This program will give us an opportunity to work on the code together
remotely, but we'll get to that after all the reading.


## Terminal
Now, you'll need a terminal.  

For windows, I'd suggest [cmdr](https://cmder.net/).  
Alternatively, if you figure that you'll be in need of a more powerful command
line experience in the future, one can't go wrong (well, ok..story for another day)
with [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)  

As a side note, VS Code has an integrated terminal, but there are some shortcomings
there we don't want to deal with down the line while developing our site (or others),
so I'd think it best to use a dedicated terminal.

On mac iTerm2 is the best option.  
Linux has a rather good terminal by default, don't change it.  

Regardless the OS, you'll want to take some time learning some [basic commands](https://towardsdatascience.com/17-terminal-commands-every-programmer-should-know-4fc4f4a5e20e).  


## Packager Manager
A packager manager manages software on your computer.  
We can install, update, remove, and all sorts of other fun stuff securely through the command line.  

For windows, use [Chocolatey](https://chocolatey.org/)  
For Mac use [Homebrew](https://brew.sh/)

Linux has a package manager by default, depending on which OS you're using.  
See the docs on that one yo.  


## Install Git (if you don't have it)
If git isn't already installed, you'll need to [install it](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).  
...yes, with the command line.  

In order to check, use the `which` command like this:  
`which git`  

If see something like looks like a path to a file, git is already installed.  
It wouldn't hurt to try and update it, I'll let you google that one one your
own (It's not hard).

So, this step we're just going to have to talk about.  
I've never met anyone that taught themselves git.  
(My old manager didn't know `git cherry-pick` was command
and he was a dev for something like 15 years, nobody "knows" git).  

That being said, you'll want to look into these commands:  
• `git init`  
• `git add`  
• `git commit`  
• `git push`  

Fortunately VS Code once again has git built in [(docs)](https://code.visualstudio.com/docs/editor/versioncontrol).  
It's actually rather good too.  
...and yes, I know that documentation is confusing af.  


## Install Hugo
Easy:  
Instruction [here](https://gohugo.io/getting-started/installing/) for all OSs.  

**NOTICE** If you are on windows, download the 'extended' version, or this site won't work.


## A GitLab account
This one is rather easy... at first.  
Open a [GitLab](https://gitlab.com/) account.  
Connect SSH keys by reading [this documentation](https://docs.gitlab.com/ee/ssh/)  

This is the step that's going to be rather awkward.  
Watch this [video](https://www.youtube.com/watch?v=54mxyLo3Mqk) first.  
Don't try to follow along, just spend the minute watching so you see what
will be poping up in the terminal as you go.  

DO NOT WORRY if you don't understand what an SSH key is, why GitLab needs it,
who cares. You need it, or it will be impossible to manage or maintain this
website (Technically you could use GitHub, but one sill needs to connect
their SSH keys on that site too).  


## Test SSH keys
Here we go! Did you make it first shot? (That'd be exceptional).  

Using you're new found knowledge of how to move around the terminal,
You'll change into a directory that you'd like to use as a test.  
This should be a new directory, ideally one you created in the terminal.  

Once in the directory you've just created, type `git init` into the terminal.  
If you don't get an error, good.  

Go to GitLab and create a new empty project. Name it anything you'd like.  
Once the project is created, there will be a blue button on the upper
right of the screen that says "clone."  

Click that button and a pop up will appear.  
The first option is "Clone with SSH," and there is a clipboard icon next to
something that looks like this:  
git@gitlab.com:ayannah/project_name.git.  
Use that clipboard icon to copy the text.  

Back into the terminal, type `git remote add origin` and paste the text you
just copied.  

Now three things.  
Start with `git add .`  
followed by `git commit -m "Initial"`  
and lastly `git push origin master`.  

If you didn't get an error, WINNER!  

and if you did, guess what?  
You're just going to have to read and google, read and google.  
Start by googling the error message itself, that will usually result in some
direction.

## Transfer the Website
What you just did to confirm the SSH keys were working as expected, we need to
do one more time.  

Much easier this time.  

Go [here]() and use the clone button again, copying the same text you did the
first time.  

In the terminal, create a folder where you will store your development projects.  
For example my `~/Documents/Dev/Hugo/` is where I keep my Hugo sites,
but yours can be just about anywhere.  

Once you've decided, use `git clone` and paste the string just like before.  

You should be able to push this to your GitLab sccount WITHOUT CREATING THE REPO
like we did last time.  

To do this use `git remote add origin git@gitlab.com:USER_NAME/PROJECT_NAME.git`
where USER_NAME is your gitlab username, and PROJECT_NAME is the name you'd like the project to be.  
You should see the same progress report as you did when you tested the SSH keys.  

Navigate to GitLab, and you should see the project there.  

The website, is now under your control.  

## Hosting
**This step is not required**  
The way the site is set up currently, it will generate a "GitLab Page."  

For now, this is perfectly acceptable hosting, however, if your site grows,
you'll absolutely need a new host.  

So, this is going to seem overly technical, and happens to be ridiculously easy
to jank up.  

Most likely, we should just talk this one through because you'll need to change 
some settings files I've not mentioned at all in this README.  

Read through this before doing anything:  
See the netlify [docs](https://docs.netlify.com/configure-builds/common-configurations/hugo/)
and the hugo [docs](https://gohugo.io/hosting-and-deployment/hosting-on-netlify/)


## Woo
That's it! We made it!  

Please keep in mind, there is no way I could write out every error or "you should see"
scenario that you might run into while running around the computer trying to figure
out just what should be happening when you installing or terminal-ing any of
the things or stuff...  

When stuck, ask!  

